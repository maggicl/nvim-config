call plug#begin('~/.local/share/nvim/site/plugged')

" Basic plugins
Plug 'joshdick/onedark.vim'
Plug 'vim-airline/vim-airline'

" custom operator commands and motions
Plug 'tpope/vim-commentary'
Plug 'kana/vim-textobj-user'
Plug 'kana/vim-textobj-entire'

" Editorconfig support
Plug 'sgur/vim-editorconfig'

" Sudo support for WW
Plug 'lambdalisue/suda.vim'

" Git integration and git grepping
Plug 'tpope/vim-fugitive'
Plug 'vim-scripts/gitignore.vim'

" Buffer / Quickfix / ... hotkeys for navigation
Plug 'tpope/vim-unimpaired'

" Quickfix and local list toggle
Plug 'Valloric/ListToggle'

" File type support
Plug 'wlangstroth/vim-racket', { 'for': 'scheme' }
Plug 'leafgarland/typescript-vim', { 'for': 'typescript' }

" Dustjs highlight
Plug 'jimmyhchan/dustjs.vim'

"LanguageServer implementation
Plug 'dense-analysis/ale'

Plug 'rbgrouleff/bclose.vim'

" Commands like :Delete :Move for both filesystem and buffers
Plug 'tpope/vim-eunuch'

" File manager
Plug 'preservim/nerdtree'
" File manager icons
Plug 'ryanoasis/vim-devicons'

" Start screen
Plug 'mhinz/vim-startify'

" Vim session management
" Vanilla sessions are described in :help 21.4
" Plug 'xolox/vim-session'
call plug#end()

