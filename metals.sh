#!/bin/sh

cd ~/.local/share/nvim/

curl -L -o ./coursier https://git.io/coursier
chmod +x ./coursier
./coursier bootstrap \
  --java-opt -Xss4m \
  --java-opt -Xms100m \
  --java-opt -Dmetals.client=LanguageClient-neovim \
  org.scalameta:metals_2.12:0.7.0 \
  -r bintray:scalacenter/releases \
  -r sonatype:snapshots \
  -o ./metals-vim -f

