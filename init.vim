" vim: set et sw=2 ts=2 tw=80 :

set rtp+=$HOME/.local/share/nvim/site

if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
  silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

source ~/.config/nvim/plugins.vim

" Use "," as leader
let mapleader = ","
let maplocalleader = " "

" FILE MANAGER

let g:dirvish_mode = ':sort ,^.*[\/],'

if !has('nvim')
  setlocal cm=blowfish2
end

" activate mouse support
set mouse=a

" allow unsaved hidden buffers
set hidden

" set column padding when scrolling from top or bottom
set scrolloff=5

" set relative line numbers
set number relativenumber

" set highlighting and continous search
set hlsearch incsearch

" show n-column limiter when textwidth is set
set colorcolumn=+1

if has('nvim')
  " prevent terminal buffers to be closed when changing buffer
  autocmd TermOpen * set bufhidden=hide

  " restore line numbers for all buffers (revert changes of cmd below)
  autocmd BufCreate * set rnu nu

  " remove line numbers to terminal
  autocmd TermOpen * set nornu nonu
end

" set autoindent on`
set autoindent

" Make :find work on all files in :pwd
set path+=**
set wildignore+=**/node_modules/**,*~

" ADDITIONAL COMMANDS

command! Q quit
command! W write
command! WQ wq

command! WW write suda://%

command! D call CreateDir()

function! CreateDir()
  !mkdir -p '%:p:h'
endfunction

" emulate system clipboard
inoremap <C-v> <ESC>"+pa
vnoremap <C-c> "+y

inoremap <S-tab> <C-d>

" make escape work in terminal and send ESC to terminal with C-Esc
tnoremap <Esc> <C-\><C-n>
tnoremap <C-Esc> <Esc>

" Automatically save folds for each file
augroup AutoSaveFolds
  autocmd!
  autocmd BufWinLeave * silent! mkview
  autocmd BufWinEnter * silent! loadview | doautocmd BufRead
augroup END

" clear search pattern with F2
noremap <F2> :let @/ = ""<CR>

" Switch between Windows with: Leader w
nnoremap <Leader>w <C-w><C-w>
nnoremap <leader>b <esc>:b <tab>

" Re-read buffer and load modelines with: Leader m
nnoremap <Leader>m :doautocmd BufRead<CR>

" Fix indentation with: Leader i
nnoremap <Leader>i mzgg=G`z

" Delete buffer with: Leader d
nnoremap <Leader>d :bp\|bd #<CR>

" move to next tab with: Leader t
tnoremap <C-e> <C-\><C-n>:tabnext<CR>

" clear trailing spaces in file
function! g:ClearTrailingSpaces()
  silent! %s/\s\s*$//g
  let @/ = "" " clear last search
endfunction

" remove trailing spaces with: Leader t s
nnoremap <silent> <Leader>s :call ClearTrailingSpaces()<CR>

" automatically remove trailing spaces when saving a file
autocmd BufWrite * call ClearTrailingSpaces()

" Reload config with: Leader R
nnoremap <Leader>R :source ~/.config/nvim/init.vim<CR>

" show Vim's true competitor with: Leader r c m
nnoremap <Leader>rcm :!open https://youtu.be/jn40Ugz0vuk<CR><CR>

" open netrw in directory of current file
nnoremap <Leader>e :Ex<CR>

" Toggle Location and Quickfix windows with these mappings
let g:lt_location_list_toggle_map = '<leader>l'
let g:lt_quickfix_list_toggle_map = '<leader>q'

" Insert in the end of file with Leader c
nnoremap <Leader>a GA

" COLORSCHEME

" set colorscheme to onedark.im for nvim and airline
colorscheme onedark
let g:airline_theme='onedark'


nnoremap - :NERDTreeToggle<CR>

syntax enable
filetype plugin indent on

if has("vms")
  set nobackup		" do not keep a backup file, use versions instead
else
  set backup		" keep a backup file (restore to previous version)
  if has('persistent_undo')
    set undofile	" keep an undo file (undo changes after closing)
  endif
endif
