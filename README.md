<!--- vim: set et ts=2 sw=2 tw=80 : -->

# nvim-config

My current neovim configuration. This is my attempt to build a vim/neovim 
configuration from scratch, without using any "framework" like *SpaceVim*

## License

Everything in this repo is licensed under WTFPL.
